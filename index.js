const express = require('express')
const app = express()
const fs = require("fs");
const ldap = require("ldapjs");
const path = require('path');
const LOGFILEJSON = path.join(__dirname,  'logfile.txt')

var serverurl = 'default'
var bindsucess = 'default'
var binderror = 'dfeault'
var clienterror = 'default'

const server = ldap.createServer();
const PORT = process.env.PORT || 1389;


// Middlewares
function authorize(req, res, next) {
  if (!req.connection.ldap.bindDN.equals("cn=root"))
    return next(new ldap.InsufficientAccessRightsError());

  return next();
}
function loadPasswdFile(req, res, next) {
  fs.readFile("passwd", "utf8", (err, data) => {
    if (err) return next(new ldap.OperationsError(err.message));

    req.users = {};

    const lines = data.split("\n");
    for (const line of lines) {
      if (!line || /^#/.test(line)) continue;

      const record = line.split(":");
      if (!record || !record.length) continue;

      req.users[record[0]] = {
        dn: "cn=" + record[0] + ", ou=users, o=myhost",
        attributes: {
          cn: record[0],
          uid: record[2],
          gid: record[3],
          description: record[4],
          homedirectory: record[5],
          shell: record[6] || "",
          objectclass: "unixUser",
        },
      };
    }

    return next();
  });
}
const pre = [authorize, loadPasswdFile];

// handling
server.bind("cn=root", (req, res, next) => {
  if (req.dn.toString() !== "cn=root" || req.credentials !== "secret")
    return next(new ldap.InvalidCredentialsError());

  res.end();
  return next();
});

server.search("o=myhost", pre, (req, res, next) => {
  const keys = Object.keys(req.users);
  console.log(keys);
  for (const k of keys) {
    if (req.filter.matches(req.users[k].attributes)) res.send(req.users[k]);
  }

  res.end();
  return next();
});
function writeLog(logdata) {
  try {
    fs.writeFileSync(LOGFILEJSON, logdata)
    console.log( "Success")

  } catch (error) {
    if (error.code === "ENOENT") {
      console.log("kein zugriff aufs logbuch")
      return "kein zugriff aufs logbuch"
    } else {
      console.log(error)
      return error.code
    }
  }
}
server.listen(PORT, () => {
  //writeLog(server.url)
  console.log("/etc/passwd LDAP server up at: %s", server.url);
  serverurl = server.url

const client = ldap.createClient({
  url: ['ldap://127.0.0.1:1389']
});

client.on('error', (err) => {
  console.log('Cleint error', err)
  clienterror = err
  // handle connection error
})

client.bind('cn=root', 'secret', (err, res) => {
  if (err) {
  console.log('bind error', err)
  binderror = err
  throw err
  }
  bindsucess = 'yes'
  console.log('bin success')
}); 
});

app.get('/', (req, res) => {
res.json({
  serverurl,
  clienterror,
  bindsucess,
  binderror,

})
})

app.listen(3000, () => {
  console.log("HTTP Server listening on 3000")
})


